const express = require('express');
const router = express.Router();

const trayController = require('../controllers/trayControllers');
const auth = require('../auth');

// add to tray
router.post('/:productID', auth.verify, trayController.addToTray);

// get all in tray
router.get('/', auth.verify, trayController.Tray);

// checkout
router.put('/checkOut', auth.verify, trayController.checkOutTray);

module.exports = router;
